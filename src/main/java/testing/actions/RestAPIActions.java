package testing.actions;

import core.Base;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.MatcherAssert;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;
import resources.Reporting;
import testing.Pages.TestPaths;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class RestAPIActions extends Base {

    public static String APIGet ( String BaseURL, String EndPoint, int ExpectedID, String ExpectedName, String ExpectedStatus, String TagForName) {
        RestAssured.baseURI = BaseURL;
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get(EndPoint);

        int ID;
        String name;
        String status;

        if (response.statusCode() == 200) {

            JsonPath jsonPathEvaluator = response.jsonPath();

            ID = jsonPathEvaluator.get("id");
            name = jsonPathEvaluator.get(TagForName);
            status = jsonPathEvaluator.get("status");

        }
      else
         {
                Reporting.TestFailed("Failed to preformed API Test");
                return Reporting.finaliseTest();
         }

        // Response validation
        if (ID == ExpectedID)
        {
            Reporting.StepPassed("successfully validated the API response ID is "+ ID);
        }
        else
        {
            Reporting.warning( "Failed to Validated the API response ID is equals to -"+ExpectedID+" - Current ID is -"+ ID);
        }

        if (name.equalsIgnoreCase(ExpectedName))
        {
            Reporting.StepPassed("successfully validated the API response name is "+ name);
        }
        else
        {
            Reporting.warning( "Failed to Validated the API response name is equals to -"+ExpectedName+" - Current Name is -"+ name);
        }

        if (status.equalsIgnoreCase(ExpectedStatus))
        {
            Reporting.StepPassed("successfully validated the API response status is "+ status);
        }
        else
        {
            Reporting.warning( "Failed to Validated the API response status is equals to -"+ExpectedStatus+" - Current status is -"+ status);
        }

        Reporting.StepPassed("successfully preformed API Test");
        return Reporting.finaliseTest();
    }


        public static String APTPut (String BaseURL, String EndPoint)
     {

        //Random Generation for ID and Name
        String RandomName = getAlphaNumericString(5);
        int RandomID = ThreadLocalRandom.current().nextInt(100,5000);
        String NewStatus = "available";

        Map jsonAsMap = new HashMap<>();

        jsonAsMap.put("id",RandomID);
            Reporting.StepPassed("Random Generated ID is "+ RandomID);
        jsonAsMap.put("name",RandomName);
            Reporting.StepPassed("Random Generated Name is "+ RandomName);
        jsonAsMap.put("status", NewStatus);
            Reporting.StepPassed("Status is available");

            Response response = given()
                .get(BaseURL + EndPoint);
        System.out.println(response.jsonPath().prettyPrint());
        Assertions.assertEquals(200, response.statusCode());

        response = given()
                .contentType("application/json")
                .body(jsonAsMap)

                .when()
                .post(BaseURL);

        Assertions.assertEquals(200, response.statusCode());
        Reporting.StepPassed("successfully validated that a new pet had been Added - Response -" + response.statusCode());

         APIGet(BaseURL,String.valueOf(RandomID),RandomID,RandomName,NewStatus,"name");
         return Reporting.finaliseTest();
    }




    // function to generate a random string of length
    public static String getAlphaNumericString(int Strlen)
    {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
                +"lmnopqrstuvwxyz!@#%&";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(Strlen);
        for (int i = 0; i < Strlen; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }

}
