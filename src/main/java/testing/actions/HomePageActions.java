package testing.actions;

import core.Base;
import resources.Reporting;
import testing.Pages.TestPaths;


import static testing.actions.RestAPIActions.getAlphaNumericString;

public class HomePageActions extends Base {

    public static String HomePage  () {

        if (!SeleniumExecutionInstance.Navigate(TestPaths.url())) {
            return Reporting.TestFailed("Failed to Navigate to" + TestPaths.url());
        }

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.HomePage())) {
            return Reporting.TestFailed("Failed to load Home Page");
        }
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.SAHyperlinkValidation())) {
            return Reporting.TestFailed("Failed to validate a Country has been selected");
        }
        Reporting.StepPassed("successfully navigated to the Home Page");
        //Reporting.StepPassedWithScreenShot("Home Page");


        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.Careers())) {
            return Reporting.TestFailed("Failed to Find Careers hyperlink");
        }
        if (!SeleniumExecutionInstance.ClickElement(TestPaths.Careers())) {
            return Reporting.TestFailed("Failed to Click the Careers hyperlink");
        }

        Reporting.StepPassed("successfully navigated to the Careers Home Page");
        Reporting.StepPassedWithScreenShot("successfully navigated to the Careers Page");

      return CareersPage();

    }

    public static String CareersPage  () {


        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.SouthAfricaHyperlink())) {
            return Reporting.TestFailed("Failed to Find South Africa Hyperlink");
        }


        if (!SeleniumExecutionInstance.ClickElement(TestPaths.SouthAfricaHyperlink())) {
            return Reporting.TestFailed("Failed to Click the South Africa Hyperlink");
        }

        Reporting.StepPassed("successfully navigated to the South Africa Job List page");
        Reporting.StepPassedWithScreenShot("successfully navigated to the South Africa Job page");

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.JobListSelection())) {
            return Reporting.TestFailed("Failed to Find a job on the South Africa Page");
        }

        return SouthAfricaJobPage();

    }

    public static String SouthAfricaJobPage () {

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.JobListSelection())) {
            return Reporting.TestFailed("Failed to Find a job on the South Africa Page");
        }

        Reporting.StepPassed("successfully Found Jobs on South Africa page");
        Reporting.StepPassedWithScreenShot("Jobs on South Africa page");

        if (!SeleniumExecutionInstance.ClickElement(TestPaths.JobListSelection()))
        {
            return Reporting.TestFailed( "Failed to Click a job on the South Africa Page");
        }

     Reporting.StepPassed("successfully click on the first available job hyperlink");

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.ApplyOnline()))
        {
            return Reporting.TestFailed( "Failed to Find the Apply Online Button");
        }

     Reporting.StepPassedWithScreenShot("successfully navigated to the Job Description page");

        if (!SeleniumExecutionInstance.ClickElement(TestPaths.ApplyOnline()))
        {
            return Reporting.TestFailed( "Failed to Click the Apply Online Button");
        }

     Reporting.StepPassed("successfully clicked on the Apply Online Button");

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.ApplyOnlineFiled()))
        {
            return Reporting.TestFailed( "Failed to Find the Apply Online Fields");
        }
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.ApplicantName()))
        {
            return Reporting.TestFailed( "Failed to Find the Applicant Name Field");
        }
        return ApplicantForm();

    }

    public static String ApplicantForm () {

        Reporting.StepPassedWithScreenShot("successfully navigated to the Job Application Fields");

        String ApplicantName = getAlphaNumericString(8);

         if (!SeleniumExecutionInstance.WaitForElement(TestPaths.ApplicantName()))
        {
            return Reporting.TestFailed( "Failed to Find the Applicant Name Field");
        }

        if (!SeleniumExecutionInstance.EnterText(TestPaths.ApplicantName(),ApplicantName))
        {
            return Reporting.TestFailed( "Failed to Enter Applicant Name");
        }

        Reporting.StepPassed("successfully entered Applicant Name - "+ApplicantName+"");

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.ApplicantPhone()))
        {
            return Reporting.TestFailed( "Failed to Find the Applicant Phone Number Field");
        }

        if (!SeleniumExecutionInstance.EnterText(TestPaths.ApplicantPhone(),SeleniumExecutionInstance.RandomTenPhoneNum()))
        {
            return Reporting.TestFailed( "Failed to Enter Applicant Phone Number");
        }

        Reporting.StepPassed("successfully entered Applicant Phone Number - "+SeleniumExecutionInstance.RandomTenPhoneNum()+"");

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.ApplicantEmail()))
        {
            return Reporting.TestFailed( "Failed to Find the Applicant Email Field");
        }
        if (!SeleniumExecutionInstance.EnterText(TestPaths.ApplicantEmail(),"automationAssessment@iLABQuality.com"))
        {
            return Reporting.TestFailed( "Failed to Enter Applicant Email");
        }

        Reporting.StepPassed("successfully entered Applicant Email - automationAssessment@iLABQuality.com");

        Reporting.StepPassedWithScreenShot("Applicant Fields Done");

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.submitButton()))
        {
            return Reporting.TestFailed( "Failed to Find the Applicant Submit Button");
        }
        if (!SeleniumExecutionInstance.ClickElement(TestPaths.submitButton()))
        {
            return Reporting.TestFailed( "Failed to Click the Submit Applicant Button");
        }

        Reporting.StepPassed("successfully Clicked the Submit Button");

        Reporting.StepPassedWithScreenShot("Application Done");

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.TextValidate()))
        {
            return Reporting.TestFailed( "Failed to Find the Applicant Validate Test");
        }

        Reporting.warning("You need to upload at least one file");

        return Reporting.finaliseTest();
    }

}
