package testing.Pages;

import org.openqa.selenium.By;

public class TestPaths {

    public static String url() {return "https://www.ilabquality.com/";}
    public static By HomePage() {return By.xpath("//div[@class='switcher notranslate']");}
    public static By SAHyperlinkValidation() {return By.xpath("//div[@class = 'selected']//a[text()=' English']");}
    public static By Careers() {return By.xpath("//div[@class='col-sm-5']//a[text()='CAREERS']");}
    public static By SouthAfricaHyperlink() {return By.xpath("//a[text()='South Africa']");}
    public static By JobListSelection() {return By.xpath("(//div[@class='wpjb-job-list wpjb-grid']//div)[1]");}
    public static By ApplyOnline() {return By.xpath("//a[text()='Apply Online ']");}
    public static By ApplyOnlineFiled() {return By.xpath("//div[@class = 'wpjb-element-input-text wpjb-element-name-applicant_name']//label[contains(text(),'Your name')]");}
    public static By ApplicantName() {return By.xpath("//div[@class = 'wpjb-element-input-text wpjb-element-name-applicant_name']//div//input[@id='applicant_name']");}
    public static By ApplicantEmail() {return By.xpath("//div[@class = 'wpjb-element-input-text wpjb-element-name-email']//div//input[@id='email']");}
    public static By submitButton() {return By.xpath("//input[@id='wpjb_submit']");}
    public static By TextValidate () {return By.xpath("//li[text()='You need to upload at least one file.']");}
    public static By ApplicantPhone() {return By.xpath("//div[@class = 'wpjb-element-input-text wpjb-element-name-phone']//div//input[@id='phone']");}




}
