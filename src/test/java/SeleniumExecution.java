import core.Base;
import org.junit.*;
import org.junit.rules.TestName;
import resources.Reporting;
import resources.WebTestDriver;
import testing.actions.HomePageActions;
public class SeleniumExecution extends Base
{

    @Rule
    public TestName name = new TestName();

    @Before
    public void PreTest()
    {
        TestName = name.getMethodName();
        Reporting.createTest();

        SeleniumExecutionInstance = new WebTestDriver(WebTestDriver.BrowserType.CHROME);

        //SeleniumExecutionInstance = new WebTestDriver(WebTestDriver.BrowserType.FIREFOX);
    }

    @After
    public void PostTest()
    {
        SeleniumExecutionInstance.shutDown();
    }

    @Test
    public void ILabSeleniumTest()
    {
        String HomePage = HomePageActions.HomePage();
            Assert.assertTrue("Result Not Good"+ HomePage,HomePage == null);

    }
}
