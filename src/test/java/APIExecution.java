import core.Base;
import org.junit.*;
import org.junit.rules.TestName;
import resources.Reporting;
import testing.actions.RestAPIActions;

public class APIExecution extends Base {

    @Rule
    public TestName name = new TestName();

    @Before
    public void PreTest()
    {
        TestName = name.getMethodName();
        Reporting.createTest();
    }

    @After
    public void PostTest()
    {
    }

    @Test
    public void ILabAPIGetTest()
    {
        String BaseUrl = "https://petstore.swagger.io/v2/pet";
        String EndPoint = "/12";

        String APIResult = RestAPIActions.APIGet(BaseUrl,EndPoint,12,"doggie","12","name");
        Assert.assertTrue("Result Not Good fir API Test"+ APIResult,APIResult == null);
    }

    @Test
    public void ILabAPIPutTest()
    {
        String BaseUrl = "https://petstore.swagger.io/v2/pet";
        String EndPoint = "/12";

        String APIResult = RestAPIActions.APTPut(BaseUrl,EndPoint);
        Assert.assertTrue("Result Not Good fir API Test"+ APIResult,APIResult == null);
    }

}
